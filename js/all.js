(function ($, app) {
    var homeCls = function () {
        this.run = function () {
            this.init();
            this.bindEvents();
        };
        
        this.init = function () {
            
        };
        
        this.bindEvents = function () {
            initslider();
            toggle_dropdown();
            fullpage_setup();
            // aos_setup();
        };
        var toggle_dropdown= function () {
            $(".hasDropDown").bind("click", '*',function (e) {
                console.log("a");
                if (document.getElementById("menuDropdown").style.display=="none"){
                    document.getElementById("menuDropdown").style.display="block";
                    document.getElementById("icon_log").style.display="none";
                    document.getElementById("icon_menu").style.display="none";
                    document.getElementById("logo").style.display="none";
                    document.getElementById("icon_close").style.display="inline-block";
                } else {
                    document.getElementById("menuDropdown").style.display="none";
                    document.getElementById("icon_log").style.display="inline-block";
                    document.getElementById("icon_menu").style.display="inline-block";
                    document.getElementById("logo").style.display="block";
                    document.getElementById("icon_close").style.display="none";

                }
                e.stopPropagation();
            });
        };
        var initslider= function(){
            var swiper = new Swiper('.swiper-container', {
                slidesPerView: 4,
                spaceBetween: 16,
                loop: true,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                autoplay: {
                    delay: 2500,
                    disableOnInteraction: false,
                },
                breakpoints:{
                    320:{
                        slidesPerView: 1,
                        spaceBetween:10,
                    },
                    600:{
                        slidesPerView: 2,
                        spaceBetween:10,
                    },
                    1200:{
                        slidesPerView: 4,
                    },
                }
            });
        };
        var fullpage_setup= function(){
            if (screen.width >1209){
                new fullpage('#fullpage', {
                    //options here
                    autoScrolling:true,
                    scrollHorizontally: true,
                    anchors:['firstPage', 'secondPage', 'thirdPage','fourthPage','fifthPage','sixthPage']
                });
            } else{
                console.log("a");
            }
            
        };
        var aos_setup= function() {
            AOS.init();
            // AOS.init({
            //     useClassNames: true,
            // }) ;
            
        };
    };
        $(document).ready(function () {
            var homeObj = new homeCls();
            homeObj.run();
            
            
        });
}(jQuery, $.app));



